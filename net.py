'''
    A collection of helper functions related to networking.
    sendmail: send a markdown formatted email
    getIP: returns the IP address (e.g. as you would get from ifconfig)
    getExternalIP: returns the IP address as seen by the world
'''
from __future__ import print_function
import string
import random
import traceback as tb
import requests
import json

def id_generator(seed, size=16, chars=string.ascii_uppercase + string.digits):
    '''Takes a seed and generates an alphanumeric hash'''
    random.seed(seed)
    return ''.join(random.choice(chars) for _ in range(size))


def sendqueue(mailqueue, host='localhost'):
    if mailqueue:
        import smtplib
        import time
        qserver = smtplib.SMTP(host)
        for mailinfo in mailqueue:
            sendmail(mailinfo[0], mailinfo[1], mailinfo[2], server=qserver)
            time.sleep(5+5*random.random())

        qserver.quit()

def ses_sendqueue(mailqueue):
    if mailqueue:
        import boto3
        import time
        ses = boto3.client('ses')
        result = []
        for mailinfo in mailqueue:
            result.append(ses_sendmail(mailinfo[0], mailinfo[1], mailinfo[2], ses=ses))
            time.sleep(.4)

def ses_sendmail(receivers, subject, body, sender=('Status Alarm', 'admin@erlichlab.org'), ses=None, cc=None, bcc=None):
    '''
        sendmail(to,subject,body,from)
            to : A tuple of tuples of name, email. e.g. (('Jeff','jeff@jeff.com'),('Bob','bob@bob.com'))
            from: A tuple ('Alarms','alarm@choji.edu')
            body: a markdown formatted email
    '''

    import email.utils
    from email.mime.text import MIMEText
    from email.mime.multipart import MIMEMultipart
    import markdown as md

    # If an email is passed in as the receiver, make it a tuple with a blank name.
    if isinstance(receivers, str):
        receivers = (('', receivers), )
    # If a single tuple is passed in make it a tuple of tuples.
    if isinstance(receivers[0], str):
        receivers = (receivers, )
    sendto = [email.utils.formataddr(x) for x in receivers]
    msg = MIMEMultipart('alternative')
    maild = {}
    maild['Destinations'] = sendto
    maild['Source'] = email.utils.formataddr(sender)
    msg['Subject'] = subject
    msg['To'] = ','.join(sendto)
    msg['From'] = email.utils.formataddr(sender)
  
    if cc:
        if isinstance(cc[0], str):
            cc = (cc, )
        msg['cc'] = ','.join([email.utils.formataddr(x) for x in cc])

    if bcc:
        if isinstance(bcc[0], str):
            bcc = (bcc, )
        msg['bcc'] = ','.join([email.utils.formataddr(x) for x in bcc])


    # Record the MIME types of both parts - text/plain and text/html.
    part1 = MIMEText(body, 'plain')
    part2 = MIMEText(md.markdown(body), 'html')

    # Attach parts into message container.
    # According to RFC 2046, the last part of a multipart message, in this case
    # the HTML message, is best and preferred.
    msg.attach(part1)
    msg.attach(part2)
    maild['RawMessage'] = {'Data': msg.as_string()}

    if not ses:
        import boto3
        ses = boto3.client('ses')
        onetime = True
    else:
        onetime = False

    try:
        result = ses.send_raw_email(**maild)
    except:
        print (result)
        tb.print_exc()

    return result

def sendslack(data,headers):
    config = __read_dbconf()
    url = config.items('slack')[0][1]
    response = requests.post(url, data=data, headers=headers)
    if response.status_code != 200:
        raise Exception(response.status_code, response.text)


def sendmattermost(channel, message, payload=None, section='mattermost'):
    '''
        sendmattermost(channel, message, payload=None, section='mattermost')
        
        Looks in .dbconf for a section (defaults to [mattermost]) to find a url for an incoming webhook.

        e.g. 
        sendmattermost('#general','Who is hungry?!?')
        sendmattermost('#private','Who is hungry?!?', section='myownhook')

        if you want to send an advanced customized payload use the payload=... to send some json.
    '''
    config = __read_dbconf()
    url = config.items(section)[0][1]
    if not payload:
        print (channel, message)
        payloaddict = {}
        payloaddict['channel'] = channel
        payloaddict['text'] = message
        request_result = requests.post(url, json=payloaddict)
    else:
        request_result = requests.post(url, data=payload)
    return request_result.ok

def __read_dbconf():
    try:
        import configparser as cp
    except ImportError:
        import ConfigParser as cp
    config = cp.ConfigParser()
    import os
    from os.path import expanduser, sep
    config.read(expanduser("~") + sep + ".dbconf")
    return config

def sendmail(receivers, subject, body, sender=('Status Alarm', 'admin@erlichlab.org'), host='localhost', debug=False, server=None):
    '''
        sendmail(to,subject,body,from)
            to : A tuple of tuples of name, email. e.g. (('Jeff','jeff@jeff.com'),('Bob','bob@bob.com'))
            from: A tuple ('Alarms','alarm@choji.edu')
            body: a markdown formatted email
    '''

    import email.utils
    from email.mime.text import MIMEText
    from email.mime.multipart import MIMEMultipart
    import markdown as md

    # If an email is passed in as the receiver, make it a tuple with a blank name.
    if isinstance(receivers, str):
        receivers = (('', receivers), )
    # If a single tuple is passed in make it a tuple of tuples.
    if isinstance(receivers[0], str):
        receivers = (receivers, )
    sendto = [email.utils.formataddr(x) for x in receivers]
    msg = MIMEMultipart('alternative')
    msg['To'] = ','.join(sendto)
    msg['From'] = email.utils.formataddr(sender)
    msg['Subject'] = subject

    # Record the MIME types of both parts - text/plain and text/html.
    part1 = MIMEText(body, 'plain')
    part2 = MIMEText(md.markdown(body), 'html')

    # Attach parts into message container.
    # According to RFC 2046, the last part of a multipart message, in this case
    # the HTML message, is best and preferred.
    msg.attach(part1)
    msg.attach(part2)

    if not server:
        import smtplib
        server = smtplib.SMTP(host)
        onetime = True
    else:
        onetime = False

    if debug:
        print (msg.as_string())
        server.set_debuglevel(True) # show communication with the server
    try:
        server.sendmail(msg['From'], msg['To'], msg.as_string())
    except:
        tb.print_exc()
    finally:
        if onetime:
            server.quit()


def getHwAddr(ifname):
    import fcntl, socket, struct
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    info = fcntl.ioctl(s.fileno(), 0x8927,  struct.pack('256s', ifname[:15]))
    return ':'.join(['%02x' % ord(char) for char in info[18:24]])

def getMacForIp(ip=None):
    import netifaces as nif

    ip = ip or getIP()

    'Returns a list of MACs for interfaces that have given IP, returns None if not found'
    for i in nif.interfaces():
        addrs = nif.ifaddresses(i)
        try:
            if_mac = addrs[nif.AF_LINK][0]['addr']
            if_ip = addrs[nif.AF_INET][0]['addr']
            if if_ip == ip:
                return if_mac
        except (IndexError, KeyError) as e: #ignore ifaces that dont have MAC or IP
            if_mac = if_ip = None

    return None


def getIP():
    '''Return the IP address that you would see in ifconfig'''
    import socket
    return ([(s.connect(('8.8.8.8', 80)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1])


def getExternalIP():
    '''use ipinfo.io to find out the world visible IP of a computer that is behind a firewall or NAT'''
    import requests
    response = requests.get('http://ipinfo.io/json')
    return str(response.json()['ip'])

if __name__ == "__main__":
    pass
    #sendmattermost('#lab-notifications', 'hey. this is a another test. please still ignore.')
    #msgbody = '''### Dear Jeff,
    #I'm writing to you about an _important_ matter. Please [click here](http://google.com)'
    #'''
    #ses_sendmail('jerlich@jerlich.com', 'testing amazon', msgbody)
