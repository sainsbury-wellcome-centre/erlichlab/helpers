''' 
    A helper class for publishing or subscribing to ZMQ messages.
'''
import zmq
import json
import random
import time


def getConf(config_section):
     
    import configparser as cp
    config = cp.ConfigParser()
    config.read(defaultConfigFile())
    assert config_section in config.sections(), "There is no section called {}. Check your .dbconf file.".format(config_section)
    return dict(config.items(config_section))

def publisher(config_section='zmq'):
    """
        Return a publish socket attached to our proxy.
    """
    ctx = zmq.Context.instance()
    pub = ctx.socket(zmq.PUB)
    pub.setsockopt(zmq.HEARTBEAT_IVL,60000)
    url = pubURL(config_section)
    pub.connect(url)
    return pub

def pusher(config_section='zmq'):
    """
        Return a publish socket attached to our proxy.
    """
    ctx = zmq.Context.instance()
    pub = ctx.socket(zmq.PUSH)
    pub.setsockopt(zmq.HEARTBEAT_IVL,60000)
    url = pushURL(config_section)
    pub.connect(url)
    return pub

def subscriber(topic, config_section='zmq'):
    """
        Return a publish socket attached to our proxy.
    """
    ctx = zmq.Context.instance()
    sub = ctx.socket(zmq.SUB)
    sub.setsockopt(zmq.HEARTBEAT_IVL,60000)
    url = subURL(config_section)
    sub.setsockopt(zmq.SUBSCRIBE, bytes(topic,'ascii'))
    sub.connect(url)
    return sub

def subURL(config_section):
    d = getConf(config_section)
    return "{}:{}".format(d['url'],d['subport'])

def pubURL(config_section):
    d = getConf(config_section)
    return "{}:{}".format(d['url'],d['pubport'])


def pushURL(config_section):
    d = getConf(config_section)
    return "{}:{}".format(d['url'],d['pushport'])



def defaultConfigFile():
    import os
    from os.path import expanduser, sep
    return expanduser("~") + sep + u".dbconf"


