import json
import numpy as np
import six

mattypes = ['int8', 'uint8', 'int16','uint16','int32','uint32','int64','uint64','single','double','logical','char',
'cell']
pytypes = [np.int8, np.uint8, np.int16, np.uint16, np.int32, np.uint32, np.int64, np.uint64, np.float32, np.float64, np.bool_, ]


def getleafinfo(leaf):
    if isinstance(leaf, np.ndarray):
    	out = (mattype(leaf.dtype), leaf.shape)
    elif isinstance(jstr, six.string_types):
    	out = ('char', len(jstr))
    	elif :
	return out


def get_info_flatten_thorough(obj):
	return 0, 1

def	get_info_flatten(obj):
	return 0, 1

def mdumps(obj, compress=True, thorough=True):
	if thorough:
		meta, obj = get_info_flatten_thorough(obj)
	else:
		meta, obj = get_info_flatten(obj)

	tojson = {'vals': obj, 'info': meta}

	out = json.dumps(tojson)

	if compress:
		out = compress(out)

	return out


def applyinfo(vals, meta):
	if 'type__' in meta.keys():
		# Then we are in a leaf node
		tsize = np.array(meta['dim__'])
		tnumel = np.prod(tsize)
		ttype = meta['type__']
		if ttype == 'cell':
			# This is a cell in matlab, which we will make a list in python
			ix = 0
			newrow = [None] * tsize[1]
			newvals = [newrow[:] for i in range(tsize[0])]
			# Preallocate the list of lists
			for col in range(tsize[1]):
				for row in range(tsize[0]):
					newvals[row][col]=applyinfo(vals[ix], meta['cell__'][ix])
					ix += 1
			elif ttype == 




def mloads(jstr, decompress=None):
	if decompress is None:
		if isinstance(jstr, six.string_types):
			decompress = False
	if decompress:
		jstr = zlib.decompress(jstr)

	big_j = json.loads(jstr)

	out  = big_j['vals']
	meta = big_j['info']

	out = applyinfo(out, meta)


