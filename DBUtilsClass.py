'''
    A helper class for accessing the database.
'''

from __future__ import print_function
import traceback as tb
import sys


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

db_loaded = False
try:
    # Try to use the native MySQLdb module if possible.
    import mariadb as db
    dbmod = 'mariadb'
    # eprint("Using MySQLdb")
    db_loaded = True
except ImportError:
    pass # try next library

if not db_loaded:
    try:
        # Try to use the native MySQLdb module if possible.
        import MySQLdb as db
        dbmod = 'mysqldb'
        # eprint("Using MySQLdb")
        db_loaded = True
    except ImportError:
        pass # try next library

if not db_loaded:
    try:
        import mysql.connector as db
        from mysql.connector import Error
        # eprint("Using mysql.connector")
        db_loaded = True
        dbmod = 'mysqlconnector'

    except ImportError:
        pass # try next one

if not db_loaded:    
    try:
        import pymysql as db
        from pymysql import Error
        dbmod = 'pymysql'
        # eprint("using pymysql")
    except ImportError:
        eprint("Neither MySQLdb nor mysql.connector nor pymysql found.  Dependency failure")
        raise ImportError('Cannot find pymysql, MySQLdb or mysql.connector module')

def defaultConfigFile():
    import os
    from os.path import expanduser, sep
    return expanduser("~") + sep + u".dbconf"

def Engine(*args):


    from sqlalchemy import create_engine
   
    if len(args) == 1 and isinstance(args[0], str):
        config_section = args[0]
        # Use that section of .dbconf
        args = None
        # set args to None to tell class to read from .dbconf
    else:
        config_section = 'client'

    try:
        import configparser as cp
    except ImportError:
        import ConfigParser as cp
    config = cp.ConfigParser()
    
    
     # This should only run if args is not None, i think
    if args is not None:
        config.add_section(config_section)
        # If passing in the info by params, put it all in a section.

    # Parse the inputs
    if not args:   # Try to use defaults in ~/.dbconf
        config.read(defaultConfigFile())
        if config_section not in config.sections():
            eprint("There is no section called %s. Check your .dbconf file." % config_section)
            raise cp.NoSectionError
        args = [None]
        try:
            args[0] = dict((x, y) for x, y in config.items(config_section))
        except (cp.NoSectionError, KeyError):
            eprint("There is no section called %s. Check your .dbconf file." % config_section)
            return None
        try:
            args[0]['port'] = int(args[0]['port'])
            # If there is a port argument, it should be an integer, not a string.
        except KeyError:
            pass
        # eprint("Loaded defaults from ~/.dbconf")
        # We should maybe check here that there is enough info to connect

    if len(args) == 1 and isinstance(args[0], dict):
        cfg = args[0]
        for key, val in zip(cfg.keys(), cfg.values()):
            config.set(config_section, key, str(val))

    elif len(args) == 1:
        raise Exception('When calling Connection with one argument it must be a dictionary')
    else:
        params = ('host', 'user', 'passwd', 'db', 'port')
        try:
            cfgdict = {}
            for param, val in zip(params, args):
                cfgdict[param] = val
                config.set(config_section, param, val)

        except IndexError:  # We might run out of arguments, since db and port are optional
            pass

        cfg = cfgdict

    if 'port' not in cfg.keys():
        cfg['port']=3306

    if 'db' not in cfg.keys():
        cfg['db']=''

    cfg['dbmod'] = dbmod

    if dbmod == "mariadb":
        return create_engine('mariadb+mariadbconnector://{user}:{passwd}@{host}:{port}/{db}'.format(**cfg), echo=False)
    else:
        return create_engine('mysql+{dbmod}://{user}:{passwd}@{host}:{port}/{db}'.format(**cfg), echo=False)


def dburl(*args):

   
    if len(args) == 1 and isinstance(args[0], str):
        config_section = args[0]
        # Use that section of .dbconf
        args = None
        # set args to None to tell class to read from .dbconf
    else:
        config_section = 'client'

    try:
        import configparser as cp
    except ImportError:
        import ConfigParser as cp
    config = cp.ConfigParser()
    
    
     # This should only run if args is not None, i think
    if args is not None:
        config.add_section(config_section)
        # If passing in the info by params, put it all in a section.

    # Parse the inputs
    if not args:   # Try to use defaults in ~/.dbconf
        config.read(defaultConfigFile())
        if config_section not in config.sections():
            eprint("There is no section called %s. Check your .dbconf file." % config_section)
            raise cp.NoSectionError
        args = [None]
        try:
            args[0] = dict((x, y) for x, y in config.items(config_section))
        except (cp.NoSectionError, KeyError):
            eprint("There is no section called %s. Check your .dbconf file." % config_section)
            return None
        try:
            args[0]['port'] = int(args[0]['port'])
            # If there is a port argument, it should be an integer, not a string.
        except KeyError:
            pass
        # eprint("Loaded defaults from ~/.dbconf")
        # We should maybe check here that there is enough info to connect

    if len(args) == 1 and isinstance(args[0], dict):
        cfg = args[0]
        for key, val in zip(cfg.keys(), cfg.values()):
            config.set(config_section, key, str(val))

    elif len(args) == 1:
        raise Exception('When calling Connection with one argument it must be a dictionary')
    else:
        params = ('host', 'user', 'passwd', 'db', 'port')
        try:
            cfgdict = {}
            for param, val in zip(params, args):
                cfgdict[param] = val
                config.set(config_section, param, val)

        except IndexError:  # We might run out of arguments, since db and port are optional
            pass

        cfg = cfgdict

    if 'port' not in cfg.keys():
        cfg['port']=3306

    if 'db' not in cfg.keys():
        cfg['db']=''

    cfg['dbmod'] = dbmod

    if dbmod == "mariadb":
        return 'mariadb+mariadbconnector://{user}:{passwd}@{host}:{port}/{db}'.format(**cfg)
    else:
        return 'mysql+{dbmod}://{user}:{passwd}@{host}:{port}/{db}'.format(**cfg)



class Connection(object):
    """This class wraps communication with a MySQL server. It will try to use
       MySQLdb as the module for connection, but will fallback to
       mysql.connector if necessary.

        Properties:
        con     The database connection.
        cur     A database cursor

        Functions:
        Connection      creates an instance of the class.
        getFromDB       a wrapper for select statements, returns the tuple from
                         cursor.fetchall()
        saveToDB        a wrapper for insert statements, returns last_insert_id
        call            a wrapper for calling mysql procedures
        execute         passes through to cur.execute for things not covered by
                         the wrapper functions
        query           An SQL query that returns the results
        saveSettings    saves your connection information to .dbconf in your
                          home directory
        use             set the default database
        checkConnection checks for connection, makes one if there is not one.
                        Use this if you have a process that runs for a long time
                        before accessing the DB again.
    """

    def __init__(self, *args):
        """ con = Connection() looks for a .dbconf file in your home directory with
            con = Connection(host, user, passwd, db , port)
                db and port are optional but if port is to be specified db should be as well.
            con = Connection(dbconf)
                where dbconf is a dictionary with user,host,passwd, database, port
                and other parameters to pass to the connect argument
                of the module.
                See http://mysql-python.sourceforge.net/MySQLdb.html#functions-and-attributes for a full list of possible connection attributes."""

        if len(args) == 1 and isinstance(args[0], str):
            self.config_section = args[0]
            # Use that section of .dbconf
            args = None
            # set args to None to tell class to read from .dbconf
        else:
            self.config_section = 'client'

        try:
            import configparser as cp
        except ImportError:
            import ConfigParser as cp
        self.config = cp.ConfigParser()

        # This should only run if args is not None, i think
        if args is not None:
            self.config.add_section(self.config_section)
            # If passing in the info by params, put it all in a section.

        # Parse the inputs
        if not args:   # Try to use defaults in ~/.dbconf
            self.config.read(defaultConfigFile())
            if self.config_section not in self.config.sections():
                eprint("There is no section called %s. Check your .dbconf file." % self.config_section)
                raise cp.NoSectionError

            args = [None]
            args[0] = dict((x, y) for x, y in self.config.items(self.config_section))
            
            try:
                args[0]['port'] = int(args[0]['port'])
                # If there is a port argument, it should be an integer, not a string.
            except KeyError:
                pass
            # eprint("Loaded defaults from ~/.dbconf")
            # We should maybe check here that there is enough info to connect

        if len(args) == 1 and isinstance(args[0], dict):
            self.cfg = args[0]
            for key, val in zip(self.cfg.keys(), self.cfg.values()):
                self.config.set(self.config_section, key, str(val))

        elif len(args) == 1:
            raise Exception('When calling Connection with one argument it must be a dictionary')
        else:
            params = ('host', 'user', 'passwd', 'db', 'port')
            try:
                cfgdict = {}
                for param, val in zip(params, args):
                    cfgdict[param] = val
                    self.config.set(self.config_section, param, val)

            except IndexError:  # We might run out of arguments, since db and port are optional
                pass

            self.cfg = cfgdict

        self.checkConnection()
    # End __init__

    def close(self):
        self.con.close()


    def checkConnection(self):
        """checkConnection()
            This function checks for a database connection and tries to get a new one
            if an open connection does not exist
        """
        try:
            self.con.ping()
            #everything is good
        except (AttributeError, db.OperationalError):
            # AttributeError is thrown if this is the first connection
            # OperationalError is thrown if the server was disconnected
            self.con = db.connect(**self.cfg)
            self.cur = self.con.cursor()


    def call(self, sqlstr, inp=None):
        '''
            call a stored procedure
            e.g.
            call('stopSession(%s)',(sessid,))
        '''
        self.execute('call ' + sqlstr, inp)
        self.commit()

    def callproc(self, sqlstr, inp=None):
        '''This does not return changed variables'''
        out = self.cur.callproc(sqlstr.partition('(')[0], inp)
            # The str.partition is in place for backward compatibility
        self.commit()
        return out

    def commit(self):
        self.con.commit()

    def saveSettings(self):
        import os, stat
        fname = defaultConfigFile()
        fid = open(fname, 'w')
        os.fchmod(fid.fileno(), stat.S_IWRITE + stat.S_IREAD)  # Make the file rw only by owner

        self.config.write(fid)
        fid.close()

    def killuser(self, user, hostpatt=None):
        self.use('information_schema')
        if hostpatt:
            sqlstr = 'select id from processlist where user=%s and host like %s'
            pids = dbc.query(sqlstr,(user, hostpatt))
        else:
            sqlstr = 'select id from processlist where user=%s'
            pids = dbc.query(sqlstr,(user, ))
            
        for pid in pids:
            dbc.execute('kill %',(pid,))


    def use(self, schema):
        self.cur.execute('use ' + schema)
        self.con.commit()

    def getFromDB(self, table, fields, wherestr='', as_dict=True):

        colstr = '`{}`'.format('`, `'.join(fields))  #Add ticks around field names
        sqlstr = "select {0} from {1}".format(colstr, table)
        if wherestr:
            sqlstr = sqlstr + " where {0}".format(wherestr)

        self.cur.execute(sqlstr)
        if as_dict:
            return dict(zip(fields, zip(*self.cur.fetchall())))
        else:
            return self.cur.fetchall()


    def lastInsertID(self):
        self.cur.execute('select last_insert_id()')
        return self.cur.fetchone()[0]

    def listEnums(self,table, column, schema=None):
        sqlstr = '''SELECT SUBSTRING(COLUMN_TYPE,5)
                    FROM information_schema.COLUMNS
                    WHERE TABLE_SCHEMA=%s 
                    AND TABLE_NAME=%s
                    AND COLUMN_NAME=%s
                '''

        if not schema:
            schema = self.query('select database()')[0][0]

        assert schema is not None, 'If a default schema is not set you must specify one.'

        out = self.query(sqlstr,(schema,table,column))
        if out:
            return out[0][0][1:-1].replace("'",'').replace('"','').split(',')
        else:
            return ''



    def saveManyToDB(self, table, cols, values):
        '''saveManyToDB(table,cols,vals)

            Table   The table to insert into
            cols    A tuple of column names to insert.
            vals    a tuple of tuples.  Each inner tuple should have as many elements as cols

            e.g. a = ((1,2,3),(4,5,6))
            c.saveManyToDB('foo',('col1','col2','col3'),a)
            '''

        sqlstr = "INSERT INTO " + table + " " + str(cols) + " VALUES " + str(values)[1:-1]
        self.cur.execute(sqlstr)
        self.commit()



    def saveToDB(self, table, mydict):

        def insert(cur,table,mydict):
            sqlstr = "insert into " + table
            colstr = " ("
            valstr = " values ("

            for k in mydict.keys():
                colstr+= "`" + k + "`" + ","
                valstr+=  "%s"  + ","

            colstr = colstr[:-1] + ")"
            valstr = valstr[:-1] + ")"
            wholestr = sqlstr + colstr + valstr
            # print wholestr
            try:
                cur.execute(wholestr,list(mydict.values()))
                return 0

            except Exception:
                eprint(wholestr)
                tb.print_exc()
                return 1

        cur=self.cur
        if isinstance(mydict, dict):
            err=insert(cur, table, mydict)
        else:
            try:
                for d in mydict:
                    err=insert(cur, table, d)
                    if err==1:
                        break
            except:
                eprint("mydict may be of the wrong type. Must be a dictionary or a sequence/list/generator of dictionaries")
                err=1
                tb.print_exc()
                raise


        if (err == 0):
            self.commit()
        else:
            self.con.rollback()
            eprint('Error saving to DB')



        return err

    def execute(self, sqlstr, vals=None):
        '''Connection.execute(sql_command)
            use this for commands that do not require any return values.
        '''
        try:
            if not vals:
                self.cur.execute(sqlstr)
            else:
                self.cur.execute(sqlstr, vals)
            self.con.commit()
        except db.Error as e:
            try:
                eprint("MySQL Error [%d]: %s" % (e.args[0], e.args[1]))
            except IndexError as e:
                eprint("MySQL Error: %s" % str(e))
            eprint(sqlstr)
            self.con.rollback()


    def query(self, sqlstr, inps=None, as_dict=False):
        '''Connection.query(sql_command)
            returns cursor.fetchall()
            Use this for commands that require return values.
        '''

        try:
            self.cur.execute(sqlstr, inps)
            out = self.cur.fetchall()
            if as_dict:
                if len(out) == 1:
                    return dict(zip([x[0] for x in self.cur.description], *out))
                else:
                    return dict(zip([x[0] for x in self.cur.description], zip(*out)))
            else:
                return out
        except:
            tb.print_exc()


    def get(self, sqlstr, inps=None):
        '''
            Rather than returning a list of rows, return a list of columns.
            e.g.
            subjid, status = dbc.get('select subjid, status from met.animals where species="rat"')
        '''
        try:
            self.cur.execute(sqlstr, inps)
            return zip(*self.cur.fetchall())
        except:
            tb.print_exc()


if __name__ == "__main__":
    dbc = Connection()
