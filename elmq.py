import pika
import configparser
import traceback as tb
import sys


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


def defaultConfigFile():
    import os
    from os.path import expanduser, sep
    return expanduser("~") + sep + u".dbconf"

class elmq(object):
    """This class wraps communication with a RabbitMQ server. 
       It required pika.

        Properties:
        channel     The connection.
        

        Functions:
        send            send a message
        recv            recieve a message         

    """

    def __init__(self, *args):
        """ mq = elmq() looks for a .dbconf file in your home directory with
            mq = elmq(host, user, passwd, port)
                db and port are optional but if port is to be specified db should be as well.
            mq = elmq(dbconf)
                where dbconf is a dictionary with user,host,passwd, database, port
                and other parameters to pass to the connect argument
                of the module.
                See http://mysql-python.sourceforge.net/MySQLdb.html#functions-and-attributes for a full list of possible connection attributes."""

        if len(args) == 1 and isinstance(args[0], str):
            self.config_section = args[0]
            # Use that section of .dbconf
            args = None
            # set args to None to tell class to read from .dbconf
        else:
            self.config_section = 'elmq'

        try:
            import configparser as cp
        except ImportError:
            import ConfigParser as cp
        self.config = cp.ConfigParser()

        # This should only run if args is not None, i think
        if args is not None:
            self.config.add_section(self.config_section)
            # If passing in the info by params, put it all in a section.

        # Parse the inputs
        if not args:   # Try to use defaults in ~/.dbconf
            self.config.read(defaultConfigFile())
            if self.config_section not in self.config.sections():
                eprint("There is no section called %s. Check your .dbconf file." % self.config_section)
                raise cp.NoSectionError

            args = [None]
            args[0] = dict((x, y) for x, y in self.config.items(self.config_section))
            
            try:
                args[0]['port'] = int(args[0]['port'])
                # If there is a port argument, it should be an integer, not a string.
            except KeyError:
                pass
            # eprint("Loaded defaults from ~/.dbconf")
            # We should maybe check here that there is enough info to connect

        if len(args) == 1 and isinstance(args[0], dict):
            self.cfg = args[0]
            for key, val in zip(self.cfg.keys(), self.cfg.values()):
                self.config.set(self.config_section, key, str(val))

        elif len(args) == 1:
            raise Exception('When calling Connection with one argument it must be a dictionary')
        else:
            params = ('host', 'user', 'passwd', 'port')
            try:
                cfgdict = {}
                for param, val in zip(params, args):
                    cfgdict[param] = val
                    self.config.set(self.config_section, param, val)

            except IndexError:  # We might run out of arguments, since db and port are optional
                pass

        self.pikacfg = {'credentials':pika.PlainCredentials(cfgdict['user'],cfgdict['passwd']),
                        'host':cfgdict['host']
                        }
        if 'port' in cfgdict:
            self.pikacfg['port'] = cfgdict['port']
            
        self.checkConnection()
    # End __init__

    def close(self):
        self.con.close()


    def checkConnection(self):
        """
            checkConnection()
            This function checks for a mq connection and tries to get a new one
            if an open connection does not exist
        """
        try:
            assert self.con.is_open
            #everything is good
        except (AttributeError, AssertionError):
            # AttributeError is thrown if this is the first connection
            # OperationalError is thrown if the server was disconnected
            self.con = pika.BlockingConnection(
                pika.ConnectionParameters(**self.pikacfg)
                )

        try:
            assert self.channel.is_open
            #everything is good
        except (AttributeError, AssertionError):
            # AttributeError is thrown if this is the first connection
            # OperationalError is thrown if the server was disconnected
            self.channel = self.con.channel()
            self.channel.queue_declare(queue='eladmin')

    def __send__(self, msg):
        '''
            __send__(msg)
            This is a generic message. 
        '''
        return self.channel.basic_publish(
            exchange='',
            routing_key='eladmin',
            body=msg)

    def __get__(self, msg, queue='eladmin'):
        '''
            __get__(msg)
            This is a generic message. 
        '''
        return self.channel.basic_get(queue)


    def consume(self, callback, queue='eladmin'):
        '''
            __consume__(msg)
            This is a generic message. 
        '''
        return self.channel.basic_consume(
            queue=queue, 
            on_message_callback=callback)


    def saveSettings(self):
        import os, stat
        fname = defaultConfigFile()
        fid = open(fname, 'w')
        os.fchmod(fid.fileno(), stat.S_IWRITE + stat.S_IREAD)  # Make the file rw only by owner

        self.config.write(fid)
        fid.close()



if __name__ == "__main__":
    mq = elmq()
