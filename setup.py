"""
    Created to form a package to use parent level imports. Utilize this by running

pip install -e .

In the root dir
"""

from setuptools import setup, find_packages

setup(
    name='helpers',
    version='1.0',
    description='Python package for installing helpers for connecting to the DB, server, and other utilities',
    author='Erlich Lab',
    url='https://gitlab.com/sainsbury-wellcome-centre/erlichlab/helpers',
    packages=find_packages()
)